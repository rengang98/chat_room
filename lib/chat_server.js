var socketio = require('socket.io')
var io
var guestNumber = 1
var nicknames = {}
var namesUsed = []
var currentRoom = {}

exports.listen = function (server) {
    io = socketio.listen(server)
    io.set('log level',1)
    io.sockets.on('connection',function (socket) {
        // console.error(socket)
        guestNumber = assignGusestName(socket,guestNumber,nicknames,namesUsed)
        joinRoom(socket,'Lobby')
        handleMessageBroadcasting(socket,nicknames)
        handleNameChangeAttempts(socket,nicknames,namesUsed)
        handleRoomJoining(socket)

        socket.on('rooms',function () {
            socket.emit('rooms',io.adapter.rooms)
        })

        handleClientDisconnection(socket,nicknames,namesUsed)
    })
}

function assignGusestName(socket, guestNumber,nicknames,namesUsed) {
    var name = 'Guest'+guestNumber
    nicknames[socket.id] = name
    socket.emit('nameResult',{
        success:true,
        name:name
    })
    namesUsed.push(name)
    return guestNumber+1
}

function joinRoom(socket,room) {
    socket.join(room)
    currentRoom[socket.id] = room
    socket.emit('joinResult',{room:room})
    socket.broadcast.to(room).emit('message',{
        text:nicknames[socket.id]+'has joined'+room+'.'
    })
    var usersInRoom = io.sockets.adapter.rooms[room]
    if(usersInRoom.length>1){
        var usersInRoomSummary = 'Users currently in ' + room + ": "
        for(var index in usersInRoom){
            var userSocketId = usersInRoom[index].id
            if(userSocketId!=socket.id){
                if(index>0){
                    usersInRoomSummary += ','
                }
                usersInRoomSummary+=nicknames[userSocketId]
            }
        }
        usersInRoomSummary += '.'
        socket.emit('message',{text:usersInRoomSummary})
    }
}

//处理用户更名请求
function handleNameChangeAttempts(socket,nickName,nameUsed) {
    socket.on('nameAttempt',function (name) {
        if(name.indexOf('Guest')==0){
            socket.emit('nameResult',{
                success:false,
                message:"name cannot begin with 'Guest'"
            })
        }else {
            if(namesUsed.indexOf(name) == -1){
                var previousName = nicknames[socket.id]
                var previousNameIndex = namesUsed.indexOf(previousName)
                namesUsed.push(name)
                nicknames[socket.id] = name
                delete namesUsed[previousNameIndex]
                socket.emit('nameResult',{
                    success:true,
                    name:name
                })
            }else {
                socket.emit('nameResult',{
                    success:false,
                    message:"Name already existed."
                })
            }
        }
    })
}

//用户发送聊天消息
function handleMessageBroadcasting(socket) {
    socket.on('message',function (message) {
        socket.broadcast.to(message.room).emit('message',{
            text: nicknames[socket.id] +':'+message.text
        })
    })
}
//更换房间
function handleRoomJoining(socket) {
    socket.on('join',function (room) {
        socket.leave(currentRoom[socket.id])
        joinRoom(socket,room.newRoom)
    })
}
function handleClientDisconnection(socket) {
    socket.on('disconnect',function () {
        var nameIndex = namesUsed.indexOf(nicknames[socket.id])
        delete nicknames[socket.id]
        delete namesUsed[nameIndex]
    })
}